package com.eterehov.interview.intersections;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class IntersectionsSearchTest {

    @Test
    public void test_findIntersections_Null() {
        List<Segment> segments = null;

        Intersection intersection = IntersectionsSearch.findIntersections(segments);

        assertNull(intersection);
    }

    @Test
    public void test_findIntersections_Empty() {
        List<Segment> segments = null;

        Intersection intersection = IntersectionsSearch.findIntersections(segments);

        assertNull(intersection);
    }

    @Test
    public void test_findIntersections_findMax_SingleZeroLengthSegment() {
        List<Segment> segments = Arrays.asList(
                new Segment(3, 3)
        );

        Intersection intersection = IntersectionsSearch.findIntersections(segments);

        assertEquals(new Intersection(3, 1), intersection);
    }

    @Test
    public void test_findIntersections_findMax_SingleSegment() {
        List<Segment> segments = Arrays.asList(
                new Segment(3, 5)
        );

        Intersection intersection = IntersectionsSearch.findIntersections(segments);

        assertEquals(new Intersection(3, 1), intersection);
    }

    @Test
    public void test_findIntersections_findMax_NotIntersectingSegments() {
        List<Segment> segments = Arrays.asList(
                new Segment(0, 2),
                new Segment(3, 5)
        );

        Intersection intersection = IntersectionsSearch.findIntersections(segments);

        assertEquals(new Intersection(0, 1), intersection);
    }

    @Test
    public void test_findIntersections_findMax_SinglePointIntersections() {
        List<Segment> segments = Arrays.asList(
                new Segment(0, 2),
                new Segment(2, 3),
                new Segment(4, 5)
        );

        Intersection intersection = IntersectionsSearch.findIntersections(segments);

        assertEquals(new Intersection(2, 2), intersection);
    }


    @Test
    public void test_findIntersections_findMax_ComplexIntersections() {
        List<Segment> segments = Arrays.asList(
                new Segment(0, 2),
                new Segment(3, 5),
                new Segment(3, 6),
                new Segment(4, 5),
                new Segment(4, 8),
                new Segment(7, 10),
                new Segment(9, 10),
                new Segment(9, 11)
        );

        Intersection intersection = IntersectionsSearch.findIntersections(segments);

        assertEquals(new Intersection(4, 4), intersection);
    }

}
