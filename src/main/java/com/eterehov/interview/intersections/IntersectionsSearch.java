package com.eterehov.interview.intersections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class IntersectionsSearch {

    private static final Comparator<Point> pointsComparator = (o1, o2) -> {
        int positionComparison = Integer.compare(o1.getPosition(), o2.getPosition());
        if (positionComparison == 0) {
            return -Boolean.compare(o1.isIncrements(), o2.isIncrements());
        }

        return positionComparison;
    };

    public static Intersection findIntersections(List<Segment> segments) {
        if (segments == null || segments.isEmpty()) {
            return null;
        }

        // Project Segments to the axis points which +1/-1 # of intersections.
        List<Point> points = new ArrayList<>(segments.size() * 2);
        for (Segment segment : segments) {
            points.add(new Point(segment.getMin(), true));
            points.add(new Point(segment.getMax(), false));
        }

        // Sort points by its "position" asc and "increments" desc.
        points.sort(pointsComparator);

        List<Intersection> intersections = new ArrayList<>();
        Integer previousPosition = null;
        int intersectionsCount = 0;
        Intersection maxIntersection = null;
        boolean incrementedOnPreviousStep = false;
        for (Point point : points) {
            if (previousPosition == null) {
                previousPosition = point.getPosition();
            }

            // check for split point
            if (previousPosition < point.getPosition() || (incrementedOnPreviousStep && !point.isIncrements())) {
                if (intersectionsCount > 0) {
                    Intersection intersection = new Intersection(previousPosition, intersectionsCount);
                    if (maxIntersection == null || maxIntersection.getIntersections() < intersectionsCount) {
                        maxIntersection = intersection;
                    }
                }

                previousPosition = point.getPosition();
            }

            if (point.isIncrements()) {
                intersectionsCount++;
                incrementedOnPreviousStep = true;
            } else {
                intersectionsCount--;
                incrementedOnPreviousStep = false;
            }
        }

        return maxIntersection;
    }

}
