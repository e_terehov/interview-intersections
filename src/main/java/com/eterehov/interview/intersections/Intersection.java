package com.eterehov.interview.intersections;

public class Intersection {

    private int position;
    private int intersections;

    public Intersection(int position, int intersections) {
        this.position = position;
        this.intersections = intersections;
    }

    public int getPosition() {
        return position;
    }

    public int getIntersections() {
        return intersections;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setIntersections(int intersections) {
        this.intersections = intersections;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Intersection that = (Intersection) o;

        if (position != that.position) return false;
        return intersections == that.intersections;
    }

    @Override
    public int hashCode() {
        int result = position;
        result = 31 * result + intersections;
        return result;
    }

    @Override
    public String toString() {
        return "Intersection{" +
                "position=" + position +
                ", intersections=" + intersections +
                '}';
    }

}
