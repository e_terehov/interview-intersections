package com.eterehov.interview.intersections;

public class Point {

    private int position;
    private boolean increments;

    public Point(int position, boolean increments) {
        this.position = position;
        this.increments = increments;
    }

    public int getPosition() {
        return position;
    }

    public boolean isIncrements() {
        return increments;
    }

    @Override
    public String toString() {
        return "Point{" +
                "position=" + position +
                ", increments=" + increments +
                '}';
    }

}
