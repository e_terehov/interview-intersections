package com.eterehov.interview.intersections;

public class Segment {

    private int min;
    private int max;

    public Segment(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Segment segment = (Segment) o;

        if (min != segment.min) return false;
        return max == segment.max;
    }

    @Override
    public int hashCode() {
        int result = min;
        result = 31 * result + max;
        return result;
    }

    @Override
    public String toString() {
        return "Segment{" +
                "min=" + min +
                ", max=" + max +
                '}';
    }

}
